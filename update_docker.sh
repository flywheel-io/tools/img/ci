#!/usr/bin/env bash
# sourced from https://gitlab.com/flywheel-io/tools/img/ci/-/blob/master/ci/update_docker.sh
replace DOCKER_VERSION=.* DOCKER_VERSION="$(latest_version git docker/engine)"
replace PUSHRM_VERSION=.* PUSHRM_VERSION="$(latest_version git christian-korneck/docker-pushrm)"

#!/usr/bin/env bash
test -z "$TRACE" || set -x
set -eu
USAGE="Usage:
  $0 [IMAGE]...

Run basic smoke tests in a docker container to verify image functionality.
"


main() {
    echo "$*" | grep -Eqvw -- "-h|--help|help" || { echo "$USAGE"; exit; }
    if [ "${CONT_MAIN:-false}" = false ]; then
        host_main "$@"
    else
        cont_main "$@"
    fi
}


host_main() {
    test $# -gt 0 || set -- flywheel/ci-alpine flywheel/ci-slim
    local TEST_STATUS=0
    for IMG in "$@"; do
        docker run --rm \
            -e "TRACE=${TRACE:-}" \
            -e "GITLAB_CI_BOT_READ_TOKEN=${GITLAB_CI_BOT_READ_TOKEN:-}" \
            -e "CONT_MAIN=true" \
            -v "$(pwd):/src" \
            "$IMG" tests/test_docker.sh "$IMG" || TEST_STATUS=1
    done
    exit "$TEST_STATUS"
}


cont_main() {
    log "Running $0 $1..."
    for BIN in docker docker-compose pre-commit; do
        quiet command -v "$BIN" || die "Command not found: $BIN"
    done
    for SCRIPT in /ci/*; do
        test -x "$SCRIPT" || die "Script is not executable: $SCRIPT"
    done
    test "$(id -u):$(id -g)" = 0:0 || die "Not running as root"

    test_latest_img
    test_update_refs
}


test_latest_img() {
    # shellcheck disable=SC1091
    . /src/ci/utils.sh

    DATE="[0-9]{8}"
    VER="[0-9]+(\.[0-9]+){1,2}"
    HASH="[0-9a-f]{8}"
    TESTS=(
        "ubuntu                 ubuntu:focal-$DATE"
        "ubuntu:latest          ubuntu:focal-$DATE"
        "ubuntu:18.04           ubuntu:bionic-$DATE"
        "ubuntu:bionic-20210325 ubuntu:bionic-$DATE"

        "python        python:$VER-buster"
        "python:latest python:$VER-buster"
        "python:3      python:$VER-buster"
        "python:3.8    python:$VER-buster"
        "python:3.8.8  python:$VER-buster"

        "python:alpine           python:$VER-alpine$VER"
        "python:3-alpine         python:$VER-alpine$VER"
        "python:3.8-alpine       python:$VER-alpine$VER"
        "python:3.8.8-alpine     python:$VER-alpine$VER"
        "python:3.8.8-alpine3.13 python:$VER-alpine$VER"

        "flywheel/lint                 flywheel/lint:master\.$HASH"
        "flywheel/lint:latest          flywheel/lint:master\.$HASH"
        "flywheel/lint:master          flywheel/lint:master\.$HASH"
        "flywheel/lint:master.cc52122b flywheel/lint:master\.$HASH"
    )
    for TEST in "${TESTS[@]}"; do
        IMAGE=$(echo "$TEST" | tr -s " " | cut -d" " -f1)
        REGEX=$(echo "$TEST" | tr -s " " | cut -d" " -f2)
        LATEST=$(_latest_img_version "$IMAGE")
        if echo "$LATEST" | grep -Eq "$REGEX"; then
            log "test_latest_img[$IMAGE -> $LATEST] pass"
        else
            die "test_latest_img[$IMAGE -> $LATEST] fail - expected $REGEX"
        fi
    done
}


test_update_refs() {
    if [ -z "${GITLAB_CI_BOT_READ_TOKEN:-}" ]; then
        log "test_update_refs skip (GITLAB_CI_BOT_READ_TOKEN not set)"
        return
    fi
    DIR=$(mktemp -d)
    cp -r /src/tests/data "$DIR"
    cd "$DIR/data"

    unset PIN_CI_REFS
    /src/ci/update_refs.sh

    grep -n "#" .gitlab-ci.yml .pre-commit-config.yaml | while read -r LINE; do
        YAML="${LINE/  \# */}"
        REGEX="${LINE/*# /}"
        if echo "$YAML" | grep -Eq "$REGEX"; then
            log "test_update_refs[$YAML] pass"
        else
            die "test_update_refs[$YAML] fail - expected $REGEX"
        fi
    done
}


# logging and formatting utilities
log() { printf "\e[32mINFO\e[0m %s\n" "$*" >&2; }
err() { printf "\e[31mERRO\e[0m %s\n" "$*" >&2; }
die() { err "$@"; exit 1; }
quiet() { "$@" >/dev/null 2>&1; }


main "$@"

# flywheel/ci-slim and flywheel/ci-alpine

**NOTE:** This image is deprecated and pending removal.
Please use [tools/img/qa-ci](https://gitlab.com/flywheel-io/tools/img/qa-ci)
as a drop-in replacement.

Images for GitLab CI with `docker`, `docker-compose` and `pre-commit` added on
top of the [python base image](https://gitlab.com/flywheel-io/tools/img/python).

Two image variants are available:

- `flywheel/ci-slim` - recommended for unit and integration testing
- `flywheel/ci-alpine` - size-optimized image for other automation jobs

## Usage

To run `update_repo.sh` on any project folder in debug mode, run:

```bash
docker run --rm -itv $(pwd):/src -w /src -e TRACE=1 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    flywheel/ci-alpine /ci/update_repo.sh
```

If your `.gitlab-ci.yml` or `.pre-commit-config.yaml` contain private repository
references, you'll need to generate a personal GitLab read
[access token](https://gitlab.com/-/profile/personal_access_tokens) and pass it
into the container with `-e GITLAB_CI_BOT_READ_TOKEN=token`.

These images are mainly used in the CI job templates in
[`tools/etc/qa-ci`](https://gitlab.com/flywheel-io/tools/etc/qa-ci).
Make sure to check out the existing job templates in case one of them can be
used - or maybe extended - before going for a custom job.

To create a custom CI job from scratch, simply set it's `image` attribute:

```yaml
test:custom:
  stage: test
  image: flywheel/ci-slim
  script:
    - test 1 = 1
```

## Development

Install the `pre-commit` hooks before committing changes:

```bash
pre-commit install
```

To build the images locally:

```bash
docker build -t flywheel/ci-slim -f Dockerfile.slim .
docker build -t flywheel/ci-alpine -f Dockerfile.alpine .
```

## Publishing

Images are published in dockerhub after every successful CI build:

- [`flywheel/ci-slim`](https://hub.docker.com/repository/docker/flywheel/ci-slim/tags?page=1&ordering=last_updated)
- [`flywheel/ci-alpine`](https://hub.docker.com/repository/docker/flywheel/ci-alpine/tags?page=1&ordering=last_updated)

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
